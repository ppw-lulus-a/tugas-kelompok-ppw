from django.urls import path
from . import views

#url for app

app_name = 'dataPelanggan'

urlpatterns = [
    path('', views.homepage, name='homepage'),
    path('show/<id>',views.show,name='show')
]