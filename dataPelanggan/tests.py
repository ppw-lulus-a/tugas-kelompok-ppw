import unittest
from django.test import TestCase, Client, override_settings
from django.urls import resolve
from django.http import HttpRequest
from .views import *

# Create your tests here.

class dataPelanggan_unit_Test(TestCase):
    @override_settings(STATICFILES_STORAGE='django.contrib.staticfiles.storage.StaticFilesStorage')
    def test_apakah_dataPelanggan_app_url_ada(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)
    
    def test_apakah_dataPelanggan_app_menggunakan_fungsi_homepage(self):
        found = resolve('/')
        self.assertEqual(found.func, homepage)

    def test_apakah_dataPelanggan_app_menggunakan_fungsi_show(self):
        found = resolve('/show/34')
        self.assertEqual(found.func, show)
    
    @override_settings(STATICFILES_STORAGE='django.contrib.staticfiles.storage.StaticFilesStorage')
    def test_apakah_terdapat_tulisan_MyLaundry_di_Homepage_html(self):
        request = HttpRequest()
        response = homepage(request)
        html_response = response.content.decode('utf8')
        self.assertIn('MyLaundry', html_response)

    @override_settings(STATICFILES_STORAGE='django.contrib.staticfiles.storage.StaticFilesStorage')
    def test_apakah_terdapat_tulisan_AboutMe_di_Homepage_html(self):
        request = HttpRequest()
        response = homepage(request)
        html_response = response.content.decode('utf8')
        self.assertIn('About Me', html_response)

    #def test_create_message_models(self):
        #test1 = dataPelanggan.objects.all().count()
        #dataPelanggan.objects.create(name='Test')
        #test2 = dataPelanggan.objects.all().count()
        #self.assertEqual(test2, test1+1)
    








