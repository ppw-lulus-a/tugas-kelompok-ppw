from django.db import models
from django import forms
from .models import dataPelanggan


class formPelanggan(forms.ModelForm):
    name = forms.CharField(widget = forms.TextInput(
        attrs = {
            "placeholder" : "Your Name"
        }
    ))

    address = forms.CharField(widget = forms.TextInput(
        attrs = {
            "placeholder" : "Address"
        }
    ))
    number = forms.IntegerField(widget = forms.TextInput(
        attrs = {
            "placeholder" : "Your Number"
        } 
    ))

    class Meta:
        model = dataPelanggan
        fields = ("name","address","number")
        labels = {
            "name" : "namaPelanggan",
            "address" : "alamatPelanggan",
            "number" : "nomorHpPelanggan"
        }