from django.shortcuts import render, redirect
from .models import dataPelanggan
from .forms import formPelanggan
from django.contrib import messages

# Create your views here.
def homepage(request):
    if request.method == "POST":       
        form = formPelanggan(request.POST)
        if form.is_valid():
            pelanggan = form.save()
            return redirect("dataPelanggan:show", pelanggan.id)
        else:
            messages.warning(request, 'Data input is not valid, Please try again!')
            return render(request, 'Homepage.html', {'homepage': form})
    else:
        form = formPelanggan()
        return render(request, 'Homepage.html', {'homepage': form, "navbar" : False })

def show(request, id):
    pelanggan = dataPelanggan.objects.get(id = id)
    nama = pelanggan.name
    return render(request,'Show.html',{'name' : nama, "navbar" : True, "id_pelanggan" : id})