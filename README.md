[![pipeline](https://gitlab.com/ppw-lulus-a/tugas-kelompok-ppw/badges/master/pipeline.svg)](http://ppw-ke12.herokuapp.com)

# Tugas Kelompok

## Description:
kelompok kami memillih ide mengenai sistem pada laundry, dimana kita tahu bahwa saat ini, banyak kalangan yang menggunakan laundry. 
sehingga sistem ini dapat mempermudah penyedia jasa laundry. sistem ini memiliki beberapa fitur sebagai berikut:
    1. pelanggan terlebih dahulu memasukan data diri berupa (nama, alamat, no telp)
    2. pelanggan akan diarahkan ke halaman order, dimana di halaman tersbeut pelanggan dapat memilih jenis laundry yang digunakan
        dan jenis pengambilan pesanan (diantar/diambil langsung), setelah itu fitur harga akan muncul secara pop up
    3.  mitra laundry dapat menolak atau menerima pesanan dan pelanggan dapat melihat status pemesanan sudah selesai atau belum
    4.  pelanggan dapat mengisi halaman feedback setelah Pesanan selesai
## Member:
- Muhammad Rafif Elfazri (1806205722)
- Fajar Anugrah Subekti (1806146940)
- Rafa Putri Ayasha (1806147136)
- Ashila Ghassani Astmara (1806205395)

## Apps Feature:
- User masuk dengan memasukkan nama sebelum memesan
- Aplikasi menghitung jumlah biaya laundry yang dikeluarkan sebelum melakukan pemesanan
- User dapat melihat feedback dari pengguna sebelumnya
- Aplikasi dapat mengganti status pemesanan laundry (admin)

## Link Heroku
http://ppw-ke12.herokuapp.com/
