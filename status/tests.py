from django.test import TestCase, Client
from dataPelanggan.models import dataPelanggan
from order.models import Order,Laundry
# Create your tests here.

class CobaTest(TestCase):
    def test_apakah_ada_slash_status(self):
        pelanggan = dataPelanggan(number = 1)
        pelanggan.save()
        response = Client().get('/status/' + str(pelanggan.id))
        self.assertEqual(response.status_code,200)

    def test_adakah_judul_pesanan_selesai(self):
        pelanggan = dataPelanggan(number = 1)
        pelanggan.save()
        response = Client().get('/status/' + str(pelanggan.id))
        self.assertContains(response, 'Pesanan Selesai')
        self.assertEqual(response.status_code,200)

    def test_adakah_judul_pesanan_diproses(self):
        pelanggan = dataPelanggan(number = 1)
        pelanggan.save()
        response = Client().get('/status/' + str(pelanggan.id))
        self.assertContains(response, 'Pesanan Diproses')
    
    def test_adakah_judul_pesanan_ditolak(self):
        pelanggan = dataPelanggan(number = 1)
        pelanggan.save()
        response = Client().get('/status/' + str(pelanggan.id))
        self.assertContains(response, 'Ditolak')






        