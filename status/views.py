from django.shortcuts import render
from dataPelanggan.models import dataPelanggan
from order.models import Order, Laundry


# Create your views here
def status(request,id):
    pelanggan = dataPelanggan.objects.get(id = id)
    Order.objects.filter(total_harga = 0).delete()
    Laundry.objects.filter(harga_laundry = 0).delete()
    order_pelanggan = Order.objects.filter(nama_pengorder = pelanggan.name).filter(alamat = pelanggan.address).filter(nomor_telepon = pelanggan.number)
    order_pelanggan_selesai = order_pelanggan.filter(status = "Pesanan Selesai")
    order_pelanggan_pending = order_pelanggan.filter(status = "Pesanan Diproses")
    order_pelanggan_ditolak = order_pelanggan.filter(status = "Ditolak")
    content = {"Pesanan_Selesai" : order_pelanggan_selesai , "Pesanan_Diproses" : order_pelanggan_pending , "Ditolak" : order_pelanggan_ditolak, "id_pelanggan" : id}
    return render ( request, "status/status.html" ,content)

    