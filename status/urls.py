from django.urls import path
from . import views

app_name = 'status'
urlpatterns = [
    path('<id>',views.status, name = 'status'),
    # path('<id>',views.laundry, name = 'laundry')
    # path('tambahPilihan/',tambahPilihan, name ="tambahPilihan")
]