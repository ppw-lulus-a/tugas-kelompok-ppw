from django.test import TestCase, Client
# from .models import Feedback
from dataPelanggan.models import dataPelanggan

class welcomeTest(TestCase):
    def ini_test_url(self) :
        customer = dataPelanggan(nama_pengorder = rafa)
        customer.save()
        c = Client()
        response = c.get('/Feedback/' + 'customer.id')
        self.assertEqual(response.status_code,200)
    
    def ini_cek_ada_button(self):
        customer = dataPelanggan(nama_pengorder = "1" )
        customer.save()
        c = Client()
        response = c.get('/Feedback/' + 'customer.id')
        content = response.content.decode('utf8') 
        self.assertIn ("<button", content)
        self.assertIn("SUBMIT", content)

    def ini_cek_ada_textbox(self):
        customer = dataPelanggan(nama_pengorder = "1" )
        response = Client().get('/Feedback/' + 'customer.id')
        content = response.content.decode('utf8')
        self.assertIn ("<textarea", content)

    def ini_cek_input(self):
        customer = dataPelanggan(nama_pengorder = "1" )
        response = Client().get('/Feedback/' + 'customer.id')
        content = response.content.decode('utf8') 
        self.assertIn ("<input", content)
