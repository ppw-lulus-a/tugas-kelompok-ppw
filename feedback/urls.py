
from django.contrib import admin
from django.urls import path
from .views import testi

app_name = 'feedback'
urlpatterns = [
    path('<id>',testi, name ='Testimoni'),
]
