from django.shortcuts import render, redirect
# from django.http import HttpResponse
from .models import Feedback
from dataPelanggan.models import dataPelanggan
from . import forms

def testi(request,id):
    if request.method == 'POST':
        pelanggan = dataPelanggan.objects.get(id = id).name
        form = forms.CommentForm(request.POST)
        if form.is_valid():
            testi = form.save()
            testi.nama = pelanggan
            testi.save()
        return redirect('feedback:Testimoni',id)
    else:
        form = forms.CommentForm()
        return render(request, 'Feedback.html', {'form': form, 'list_testimoni' : Feedback.objects.all(), 'id_pelanggan' : id})