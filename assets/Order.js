function calculate(){
    var berat = document.getElementById("id_berat_paket").value
    var kategori = document.getElementById("id_category").innerHTML
    var paket = document.getElementById("id_nama_paket").innerHTML
    var harga = 0
    if(paket === "Reguler"){
        harga += berat * 6000
    }
    else if(paket === "Express"){
        harga += berat * 8000
    }
    if(kategori == "diantarkan" && berat > 0){
        harga += 5000
    }
    document.getElementById("id_harga_laundry").value = harga
    document.getElementById("hasil-kalkulasi").style.display = ""
}

function pilihReguler(){
    document.getElementById("id_nama_paket").value = "Reguler"
    document.getElementById("tombol_reguler").disabled = true
    document.getElementById("tombol_express").disabled = false
}

function pilihExpress(){
    document.getElementById("id_nama_paket").value = "Express"
    document.getElementById("tombol_reguler").disabled = false
    document.getElementById("tombol_express").disabled = true
}
