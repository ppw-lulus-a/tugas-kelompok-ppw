from django.test import TestCase, Client
from .models import Order, Laundry
from .views import order, laundry
from .forms import LaundryForm, OrderForm
from dataPelanggan.models import dataPelanggan

# Create your tests here.

class OrderTest(TestCase):
    #TestCase Rafif test Order
    def test_url_order_exists(self):
        pelanggan = dataPelanggan(number = 12)
        pelanggan.save()
        c = Client()
        response = c.get('/order/'+ str(pelanggan.id))
        self.assertEqual(response.status_code, 200)
    
    def test_url_order_form_exists(self):
        pelanggan = dataPelanggan(number = 12)
        pelanggan.save()
        c = Client()
        response = c.get('/order/'+ str(pelanggan.id))
        content = response.content.decode('utf8') 
        self.assertIn('<form', content)
    
    def test_url_laundry_form_exists(self):
        pelanggan = dataPelanggan(number = 12)
        pelanggan.save()
        pesanan = Order()
        pesanan.save()
        c = Client()
        response = c.get('/order/'+ str(pelanggan.id) + '/' + str(pesanan.id))
        content = response.content.decode('utf8') 
        self.assertIn('<form', content)

    def test_url_order_laundry_exist(self):
        pelanggan = dataPelanggan(number = 12)
        pelanggan.save()
        pesanan = Order()
        pesanan.save()
        c = Client()
        response = c.get('/order/'+ str(pelanggan.id) + '/' + str(pesanan.id))
        self.assertEqual(response.status_code, 200)
    
    def test_post_hal_laundry(self):
        pelanggan = dataPelanggan(number = 12)
        pelanggan.save()
        pesanan = Order()
        pesanan.save()
        link = '/order/'+ str(pelanggan.id) + '/' + str(pesanan.id)
        c = Client()
        response = c.post(link, {
            'berat_paket': 6,
            'harga_laundry': 41000,
        })
        self.assertEqual(response.status_code, 302)
    
    def test_post_hal_order(self):
        pelanggan = dataPelanggan(number = 12)
        pelanggan.save()
        link = '/order/'+ str(pelanggan.id)
        c = Client()
        response = c.post(link, {
            'category': 'diantarkan',
            'nama_paket': 'Reguler',
        })
        self.assertEqual(response.status_code, 302)
    
    def test_form_order_berhasil(self):
        form_data = {
            'nama_paket': "Reguler",
            'category': "diantarkan", 
        }
        order_form = OrderForm(data=form_data)
        self.assertTrue(order_form.is_valid())
    
    def test_form_laundry_berhasil(self):
        form_data = {
            'harga_laundry': 2000,
            'berat_paket': 6000, 
        }
        laundry_form = LaundryForm(data=form_data)
        self.assertTrue(laundry_form.is_valid())
    
    def test_total_harga(self):
        pelanggan = dataPelanggan(number = 12)
        pelanggan.save()
        order = Order(total_harga = 41000)
        order.save()
        c = Client()
        response = c.get(
            '/order/'+ str(pelanggan.id) + '/' + str(order.id),
        )
        self.assertIn("41000", response.content.decode())

    def test_tambah_total_harga(self):
        pelanggan = dataPelanggan(number = 12)
        pelanggan.save()
        order = Order(total_harga = 41000)
        order.save()
        c = Client()
        response = c.post(
            '/order/'+ str(pelanggan.id) + '/' + str(order.id),
            {"harga_laundry" : 41000,
            'berat_paket' : 6}
        )
        response2 = c.get(
            '/order/'+ str(pelanggan.id) + '/' + str(order.id),
        )

        self.assertIn("82000", response2.content.decode())
    
    def test_laundry_reguler_diambil(self):
        pelanggan = dataPelanggan(number = 12)
        pelanggan.save()
        order = Order(category = 'ambil_sendiri')
        order.save()
        c = Client()
        response = c.post(
            '/order/'+ str(pelanggan.id) + '/' + str(order.id),
            {"harga_laundry" : 0,
            'berat_paket' : 6}
        )
        response2 = c.get(
            '/order/'+ str(pelanggan.id) + '/' + str(order.id),
        )

        self.assertIn("36000", response2.content.decode())
    
    def test_laundry_express_diambil(self):
        pelanggan = dataPelanggan(number = 12)
        pelanggan.save()
        order = Order(nama_paket = 'Express',category = 'ambil_sendiri')
        order.save()
        c = Client()
        response = c.post(
            '/order/'+ str(pelanggan.id) + '/' + str(order.id),
            {"harga_laundry" : 0,
            'berat_paket' : 6}
        )
        response2 = c.get(
            '/order/'+ str(pelanggan.id) + '/' + str(order.id),
        )

        self.assertIn("48000", response2.content.decode())
    
    def test_laundry_express_diantarkan(self):
        pelanggan = dataPelanggan(number = 12)
        pelanggan.save()
        order = Order(nama_paket = 'Express')
        order.save()
        c = Client()
        response = c.post(
            '/order/'+ str(pelanggan.id) + '/' + str(order.id),
            {"harga_laundry" : 0,
            'berat_paket' : 6}
        )
        response2 = c.get(
            '/order/'+ str(pelanggan.id) + '/' + str(order.id),
        )

        self.assertIn("53000", response2.content.decode())




        
        
        

    #Test Fajar
