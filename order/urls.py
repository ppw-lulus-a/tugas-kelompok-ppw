from django.urls import path
from . import views

app_name = 'order'
urlpatterns = [
    path('<id>',views.order, name = 'order'),
    path('<id>/<id_order>',views.laundry, name = 'laundry')
]