from django.contrib import admin

# Register your models here.
from .models import Order, Laundry
admin.site.register(Laundry)
admin.site.register(Order)