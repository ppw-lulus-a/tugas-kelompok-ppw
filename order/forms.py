from django import forms
from . import models
# from django.forms.fields import ChoiceField

# class DropPick(forms.Form):
class OrderForm(forms.ModelForm):
    class Meta:
        model = models.Order
        fields = ['nama_paket', 'category']
        
class LaundryForm(forms.ModelForm):
    class Meta:
        model = models.Laundry
        # fields = ['nama_laundry', 'berat_paket']
        fields = ['berat_paket', 'harga_laundry']
        widgets = {
            'harga_laundry' : forms.NumberInput(attrs = {'readonly' : 'readonly'}),
        }
        