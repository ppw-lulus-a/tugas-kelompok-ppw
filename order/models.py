from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator 

# Create your models here.
class Laundry(models.Model):
    berat_paket =  models.PositiveIntegerField(default=5)
    harga_laundry = models.IntegerField(default = 0)

class Order(models.Model):
    nama_pengorder = models.CharField(max_length = 20, default = "Jane Doe")
    nama_paket = models.CharField(max_length = 20, default = "Reguler")
    alamat = models.CharField(max_length = 50, default = "KOS Murah")
    nomor_telepon = models.IntegerField(default = "082178150093")
    total_harga = models.IntegerField(default = 0)
    DROP_CHOICES = (('diantarkan','Diantarkan'),('ambil_sendiri','Ambil Sendiri'))
    category = models.CharField(max_length=20, choices=DROP_CHOICES, default = 'diantarkan')
    status = models.CharField(default = "Menunggu Verifikasi", max_length = 30)
    laundry = models.ManyToManyField(Laundry)








    
    


