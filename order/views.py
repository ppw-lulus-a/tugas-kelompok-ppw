from django.shortcuts import render, redirect
from . import forms
from . import models
from dataPelanggan.models import dataPelanggan
from django.views.decorators.http import require_POST


# Create your views here.

def order(request, id):
    if request.method == "POST":
        form_order = forms.OrderForm(request.POST)
        if form_order.is_valid():
            pelanggan = dataPelanggan.objects.get(id = id)
            order_baru = form_order.save()
            order_baru.nama_pengorder = pelanggan.name
            order_baru.alamat = pelanggan.address
            order_baru.nomor_telepon = pelanggan.number
            order_baru.save()
        return redirect("order:laundry", id ,order_baru.id)
    else:
        form_order = forms.OrderForm()
        content = {'form_order' : form_order, "id_pelanggan" : id} 
        return render(request ,'order/order.html', content)

def laundry(request, id ,id_order):
    if models.Order.objects.get(id = id_order).status != "Menunggu Verifikasi":
        return redirect("order:order", id)
    if request.method == "POST":
        form_laundry = forms.LaundryForm(request.POST)
        if form_laundry.is_valid():
            laundry_baru = form_laundry.save()
            order_mau_ditambah = models.Order.objects.get(id = id_order)
            if(laundry_baru.berat_paket > 0):
                harga = 0
                if order_mau_ditambah.nama_paket == "Reguler":
                    harga += laundry_baru.berat_paket * 6000
                elif order_mau_ditambah.nama_paket == "Express":
                    harga += laundry_baru.berat_paket * 8000
                if order_mau_ditambah.category == "diantarkan":
                    harga += 5000
                laundry_baru.harga_laundry = harga
                laundry_baru.save()
                order_mau_ditambah.laundry.add(laundry_baru)
                order_mau_ditambah.total_harga += laundry_baru.harga_laundry
                order_mau_ditambah.save()
            else:
                laundry_baru.delete()
        return redirect("order:laundry", id,order_mau_ditambah.id)
    else:
        form_laundry = forms.LaundryForm()
        order_mau_ditambah = models.Order.objects.get(id = id_order)
        total_harga = order_mau_ditambah.total_harga
        paket = order_mau_ditambah.nama_paket
        kategori = order_mau_ditambah.category 
        content = {
            "form_laundry" : form_laundry, 
            "nama_paket" :paket,
            "total_harga" : total_harga,
            "kategori" : kategori,
            "id_pelanggan" : id}
        return render(request, "order/laundry.html", content)

# @require_POST
# def tambahPilihan(request):
#     form = forms.LaundryForm(request.POST)
#     form2 = forms.Drop(request.POST)
    
#     if forms.Drop() == 'Diantarkan':
#         # data same day ditambahkan dengan ini 
