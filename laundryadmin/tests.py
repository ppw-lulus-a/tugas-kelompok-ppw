from django.test import TestCase, Client
from order.models import Laundry, Order
from .views import cancel_order, finish_order, accept_order

# Create your tests here.
class LaundryAdminTest(TestCase):
    def test_url_exist(self):
        c = Client()
        response = c.get('/laundryadmin/')
        self.assertEqual(response.status_code, 200)
    
    def test_ada_gross_income(self):
        c = Client()
        response = c.get('/laundryadmin/')
        self.assertIn("Gross Income", response.content.decode())
    
    def test_gross_income_work(self):
        order = Order(total_harga = 21000, status = "Pesanan Selesai")
        order.save()
        c = Client()
        response = c.get('/laundryadmin/')
        self.assertIn("21000", response.content.decode())
        order1 = Order(total_harga = 21000, status = "Pesanan Selesai")
        order1.save()
        response = c.get('/laundryadmin/')
        self.assertIn("42000", response.content.decode())
    
    def test_finish_button(self):
        order = Order(total_harga = 21000)
        order.save()
        c = Client()
        response = c.get('/laundryadmin/selesai/' + str(order.id) + '/')
        response1 = c.get('/laundryadmin/')
        self.assertIn("Pesanan Selesai", response1.content.decode())
    
    def test_accept_button(self):
        order = Order(total_harga = 21000)
        order.save()
        c = Client()
        response = c.get('/laundryadmin/terima/' + str(order.id) + '/')
        response1 = c.get('/laundryadmin/')
        self.assertIn("Pesanan Diproses", response1.content.decode())
    
    def test_cancel_button(self):
        order = Order(total_harga = 21000)
        order.save()
        c = Client()
        response = c.get('/laundryadmin/cancel/' + str(order.id) + '/')
        response1 = c.get('/laundryadmin/')
        self.assertNotIn("Pesanan Diproses", response1.content.decode())
        self.assertNotIn("Pesanan Selesai", response1.content.decode())
        self.assertNotIn("Menunggu Verifikasi", response1.content.decode())
        
        


