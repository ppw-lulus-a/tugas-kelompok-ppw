from django.apps import AppConfig


class LaundryadminConfig(AppConfig):
    name = 'laundryadmin'
