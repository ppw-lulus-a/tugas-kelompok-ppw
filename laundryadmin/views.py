from django.shortcuts import render, redirect
from order.models import Order, Laundry

# Create your views here.
def admin(request):
    Order.objects.filter(total_harga = 0).delete()
    Laundry.objects.filter(harga_laundry = 0).delete()
    order_complete = Order.objects.filter(status = "Pesanan Selesai")
    total_pendapatan = 0
    for order in order_complete:
        total_pendapatan += order.total_harga
    order_not_complete = Order.objects.exclude(status = "Pesanan Selesai").exclude(status = "Ditolak")
    content = {"order_no" : order_not_complete, "order_yes": order_complete, "pendapatan":total_pendapatan}
    return render(request, "laundryadmin/admin.html", content)

def accept_order(request, id):
    order = Order.objects.get(id = id)
    order.status = "Pesanan Diproses"
    order.save()
    return redirect("laundryadmin:admin")

def finish_order(request, id):
    order = Order.objects.get(id = id)
    order.status = "Pesanan Selesai"
    order.save()
    return redirect("laundryadmin:admin")

def cancel_order(request, id):
    order = Order.objects.get(id = id)
    order.status = "Ditolak"
    order.save()
    return redirect("laundryadmin:admin")