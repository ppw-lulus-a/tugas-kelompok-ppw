from django.urls import path
from . import views

app_name = 'laundryadmin'
urlpatterns = [
    path('',views.admin, name = 'admin'),
    path('terima/<id>/',views.accept_order, name = 'accept'),
    path('selesai/<id>/', views.finish_order, name = 'finish'),
    path('tolak/<id>/', views.cancel_order, name = 'cancel'),
]